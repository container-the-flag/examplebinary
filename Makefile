BASENAME := examplebinary
CC=gcc
CFLAGS=-m32 -fno-stack-protector

build:
	$(CC) $(CFLAGS) $(BASENAME).c -o $(BASENAME)

test: build
	python solve.py --host process

clean:
	rm $(BASENAME)
