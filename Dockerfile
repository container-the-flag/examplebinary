FROM ubuntu:latest
LABEL author="Nicklaus McClendon"
LABEL version="1.0.0"
ENV  TERM='linux' TERMINFO='/etc/terminfo'

RUN \
  apt-get update && \
  DEBIAN_FRONTEND=noninteractive \
  apt-get -y install --no-install-recommends \
  gcc-multilib \
  make \
  python-dev \
  python-pip \
  python-setuptools \
  xinetd \
  && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/
COPY requirements.txt /health/requirements.txt
RUN pip install -r /health/requirements.txt

COPY examplebinary.c Makefile /app/
WORKDIR /app
RUN make

COPY examplebinary.conf /etc/xinetd.d/examplebinary
COPY flag.txt /flag.txt

COPY solve.py /health/solve.py

HEALTHCHECK --interval=5s --timeout=2s --retries=12 \
  CMD ["python", "/health/solve.py", "--flag", "/flag.txt"]

EXPOSE 1337

ENTRYPOINT ["xinetd"]
CMD ["-dontfork"]
