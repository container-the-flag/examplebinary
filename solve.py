import click
import sys
from pwnlib.tubes.process import process
from pwnlib.tubes.remote import remote

PAYLOAD = 'a' * 128 + '\n'

def solve(tube):
    flag = tube.recv().strip()
    return flag

def test_process(tube, flag):
    expected_flag = ''
    with open(flag) as flag_file:
        expected_flag = flag_file.read().strip()
    found_flag = solve(tube)
    return expected_flag == found_flag

@click.command()
@click.option('--host', default='localhost', help='Remote host')
@click.option('--port', default=1337, help='Remote port')
@click.option('--flag', default='./flag.txt', help='Where the flag file is located')
def main(host, port, flag):
    if host == 'process':
        tube = process('./examplebinary')
    else:
        tube = remote(host, port)
    success = test_process(tube, flag)
    if success:
        sys.exit(0)
    else:
        sys.exit(1)

if __name__ == '__main__':
    main()
